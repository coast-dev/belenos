# -*- coding: utf-8 -*-
# This file is part of the 'Belenos' Python package.
#
# Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
#
#  FREE SOFTWARE LICENCING
#  -----------------------
# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
# software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
# CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
# and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
# and the software's author, the holder of the economic rights, and the successive licensors have only limited
# liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
# and/or developing or reproducing the software by the user in light of its specific status of free software, that may
# mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
# experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
# to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
# you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
#
#
# COMMERCIAL SOFTWARE LICENCING
# -----------------------------
# You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
# negotiate a specific contract with a legal representative of CEA.
#
from __future__ import unicode_literals
from setuptools import setup, find_packages


setup(
    name="Belenos",
    version="0.0.1",  # Also found in belenos.__init__.py and in doc/source/conf.py
    install_requires=[
        'h5py>=2.10.0',
        'numpy>=1.16.4',
        'Pillow>=6.2.1',
        'pandas>=0.24.2'
    ],
    python_requires='>=3.6, <4',  # 3.6/3.7/3.8/3.9
    packages=find_packages(include=['belenos', 'belenos.*']),

    # Metadata to display on PyPI
    author="Damien CHAPON",
    description="Adaptive Mesh Refinement (AMR) astrophysical simulation data post-processing package",
    long_description="""TODO""",
    author_email="damien.chapon@cea.fr",
    license="CEA CNRS Inria Logiciel Libre License, version 2.1 (CeCILL-2.1, http://www.cecill.info)",

    keywords="Astrophysics simulation analysis visualization AMR",
    url="https://belenos.readthedocs.io",
    download_url="https://gitlab.com/coast-dev/belenos",
    project_urls={
        "Source": "https://drf-gitlab.cea.fr/coast-dev/belenos/",
    },

    classifiers=["Intended Audience :: Science/Research",
                 "License :: OSI Approved :: CEA CNRS Inria Logiciel Libre License, version 2.1 (CeCILL-2.1)",
                 "Operating System :: MacOS :: MacOS X",
                 "Operating System :: POSIX :: Linux",
                 "Development Status :: 3 - Alpha",
                 # "Development Status :: 4 - Beta",
                 # "Development Status :: 5 - Production/Stable",
                 "Programming Language :: C++",
                 "Programming Language :: Python :: 3.6",
                 "Programming Language :: Python :: 3.7",
                 "Programming Language :: Python :: 3.8",
                 "Programming Language :: Python :: 3.9",
                 "Topic :: Software Development :: Version Control :: Git",
                 "Topic :: Documentation :: Sphinx",
                 "Intended Audience :: Science/Research",
                 "Topic :: Scientific/Engineering :: Physics",
                 "Topic :: Scientific/Engineering :: Astronomy",
                 "Topic :: Scientific/Engineering :: Visualization",
                 "Topic :: Software Development :: Libraries :: Python Modules",
                 "Topic :: Scientific/Engineering :: Information Analysis"],
)
